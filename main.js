// ESM syntax is supported.
import express from "express";
import scoresJSON from "./data/user.json";

const app = express();

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.json("change url to 'http://localhost:4000/scores' to see all of the scores!");
});

app.get("/scores", (req, res) => {
  let scores = scoresJSON.sort((a, b) => (a.score > b.score ? -1 : 1)).slice(0, 3);

  res.status(200).json(scores);
});

app.post("/scores", (req, res) => {
  const highscorer = req.body;
  scoresJSON.push(highscorer);
  res.status(201).json(scoresJSON);
});

app.get("*", (req, res) => {
  res.status(404).json({
    message: "You lost us there, try a different url",
  });
});

app.listen(4000, () => {
  console.log("express server is running on port 4000");
});
